import React from "react";
import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  Stack,
  Collapse,
  Icon,
  Link,
  Image,
  Popover,
  PopoverTrigger,
  PopoverContent,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
} from "@chakra-ui/react";
import {
  HamburgerIcon,
  CloseIcon,
  ChevronDownIcon,
  ChevronRightIcon,
} from "@chakra-ui/icons";

//img
import XLAFF_LOGO from "../../Assets/img/XLAFF-Logo.png";
import EN from "../../Assets/img/EN.png";

export default function WithSubnavigation() {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box position="fixed" width="100%">
      <Flex
        bg="black"
        color="white"
        minH={"10vh"}
        py={{ base: 2 }}
        px={{ base: 4 }}
        align={"center"}
        fontFamily={"aAtmospheric"}
      >
        <Flex flex={{ base: 1 }} justify={{ base: "start", xl: "center" }}>
          <Image height="10vh" src={XLAFF_LOGO} />
          <Flex
            display={{ base: "none", xl: "flex" }}
            alignItems={"center"}
            ml={10}
          >
            <DesktopNav />
          </Flex>
          <Stack
            display={{ base: "none", xl: "inline-flex" }}
            justify={"flex-end"}
            direction={"row"}
            spacing={3}
            ml={10}
            alignItems={"center"}
          >
            <Link href="/Login">
              <Button
                fontSize={"sm"}
                color={"white"}
                variant={"link"}
                // href={"#"}
                _hover={{
                  textDecoration: "none",
                  color: "gray.800",
                }}
              >
                LOGIN
              </Button>
            </Link>
            <Link href="/Register">
              <Button
                display={{ base: "none", md: "inline-flex" }}
                fontSize={"sm"}
                // fontWeight={600}
                color={"white"}
                variant={"link"}
                href={"#"}
                _hover={{
                  textDecoration: "none",
                  color: "gray.800",
                }}
              >
                REGISTER
              </Button>
            </Link>
            <Image height="5vh" src={EN} />
          </Stack>
        </Flex>
        <Flex display={{ base: "flex", xl: "none" }}>
          <IconButton
            onClick={onToggle}
            icon={
              isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />
            }
            variant={"ghost"}
            aria-label={"Toggle Navigation"}
          />
        </Flex>
      </Flex>

      <Collapse in={isOpen} animateOpacity>
        <MobileNav />
      </Collapse>
    </Box>
  );
}

const DesktopNav = () => {
  const linkColor = useColorModeValue("white", "gray.200");
  const linkHoverColor = useColorModeValue("gray.800", "white");
  const popoverContentBgColor = useColorModeValue("white", "gray.800");

  return (
    <Stack direction={"row"} spacing={4}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={"hover"} placement={"bottom-start"}>
            <PopoverTrigger>
              <Link
                p={2}
                href={navItem.href ?? "#"}
                fontSize={"sm"}
                fontWeight={500}
                color={linkColor}
                _hover={{
                  textDecoration: "none",
                  color: linkHoverColor,
                }}
              >
                {navItem.label}
              </Link>
            </PopoverTrigger>

            {navItem.children && (
              <PopoverContent
                border={0}
                boxShadow={"xl"}
                bg={popoverContentBgColor}
                p={4}
                rounded={"xl"}
                minW={"sm"}
              >
                <Stack>
                  {navItem.children.map((child) => (
                    <DesktopSubNav key={child.label} {...child} />
                  ))}
                </Stack>
              </PopoverContent>
            )}
          </Popover>
        </Box>
      ))}
    </Stack>
  );
};

const DesktopSubNav = ({ label, href, subLabel }) => {
  return (
    <Link
      href={href}
      role={"group"}
      display={"block"}
      p={2}
      rounded={"md"}
      _hover={{ bg: useColorModeValue("pink.50", "gray.900") }}
    >
      <Stack direction={"row"} align={"center"}>
        <Box>
          <Text
            transition={"all .3s ease"}
            _groupHover={{ color: "pink.400" }}
            fontWeight={500}
          >
            {label}
          </Text>
          <Text fontSize={"sm"}>{subLabel}</Text>
        </Box>
        <Flex
          transition={"all .3s ease"}
          transform={"translateX(-10px)"}
          opacity={0}
          _groupHover={{ opacity: "100%", transform: "translateX(0)" }}
          justify={"flex-end"}
          align={"center"}
          flex={1}
        >
          <Icon color={"pink.400"} w={5} h={5} as={ChevronRightIcon} />
        </Flex>
      </Stack>
    </Link>
  );
};

const MobileNav = () => {
  return (
    <Stack
      bg="black"
      p="0rem 1.2rem"
      display={{ xl: "none" }}
      height="100vh"
      width={{ base: "40vw", sm: "30vw" }}
      fontSize={{ base: "3vw", sm: "2vw" }}
    >
      {NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  );
};

const MobileNavItem = ({ label, children, href }) => {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Flex
        py={2}
        as={Link}
        href={href ?? "#"}
        justify={"space-between"}
        align={"center"}
        _hover={{
          textDecoration: "none",
        }}
      >
        <Text fontWeight={600} color="white">
          {label}
        </Text>
        {children && (
          <Icon
            as={ChevronDownIcon}
            transition={"all .25s ease-in-out"}
            transform={isOpen ? "rotate(180deg)" : ""}
            w={6}
            h={6}
          />
        )}
      </Flex>

      <Collapse in={isOpen} animateOpacity style={{ marginTop: "0!important" }}>
        <Stack
          mt={2}
          pl={4}
          borderLeft={1}
          borderStyle={"solid"}
          borderColor={useColorModeValue("gray.200", "gray.700")}
          align={"start"}
        >
          {children &&
            children.map((child) => (
              <Link key={child.label} py={2} href={child.href}>
                {child.label}
              </Link>
            ))}
        </Stack>
      </Collapse>
    </Stack>
  );
};

const NAV_ITEMS = [
  {
    label: "HOME",
    href: "/Home",
  },
  {
    label: "EXPLORE",
    href: "/Explore",
  },
  {
    label: "METADROP",
    href: "/Metadrop",
  },
  {
    label: "EXCHANGE",
    href: "/Exchange",
  },
  {
    label: "GAMEFI",
    href: "/GameFi",
  },
  {
    label: "NFT MARKET",
    href: "/NFTMarket",
  },
];
