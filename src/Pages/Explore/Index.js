import React from "react";
import {
  Stack,
  Flex,
  Button,
  Text,
  Image,
  VStack,
  useBreakpointValue,
  Box,
  Container,
} from "@chakra-ui/react";
import { SocialIcon } from "react-social-icons";

//img
import XLAFF_LOGO from "../../Assets/img/XLAFF-Logo.png";
import HOME_BG_1 from "../../Assets/img/HOME-BG-1.png";
import HOME_BG_2 from "../../Assets/img/HOME-BG-2.png";
import HOME_BG_3 from "../../Assets/img/HOME-BG-3.png";

export default function Index() {
  return (
    <Box w={"full"} paddingTop="10vh">
      <Box
        w={"full"}
        h={"100vh"}
        backgroundImage={HOME_BG_2}
        backgroundSize={"cover"}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"center"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3vw", md: "3vw", xl: "2vw" }}
            mb="2rem"
          >
            <Text>Explore</Text>
            <Text>The metaverse</Text>
          </Text>

          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "2vw", md: "1.7vw", xl: "1vw" }}
            mb={{ base: "0.5rem", md: "2rem" }}
          >
            <Text>
              The interconnected ecosystem of games, communities, and NFT
              assets. XLAFF maximizes players' benefits and enables
              interoperability across the Metaverse.
            </Text>
          </Text>
          <Box w={"full"} mb={{ base: "0.5rem", md: "2rem" }}>
            <Flex justify={"center"} mt={{ base: "0.6rem", md: "1rem" }}>
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#FFC300"
                style={{
                  marginRight: "0.6rem",
                  width: "2rem",
                  height: "2rem",
                }}
              />
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#FFC300"
                style={{
                  marginRight: "0.6rem",
                  width: "2rem",
                  height: "2rem",
                }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#FFC300"
                style={{
                  marginRight: "0.6rem",
                  width: "2rem",
                  height: "2rem",
                }}
              />
            </Flex>
          </Box>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3vw", md: "2.5vw", xl: "1.5vw" }}
            mb={{ base: "10rem", md: "20rem" }}
            textAlign={"start"}
          >
            <Text>/ Game List</Text>
          </Text>
        </Container>
      </Box>
      <Box
        w={"full"}
        // h={"100vh"}
        bg={"black"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pb={{ base: "2rem", md: "3rem" }}
        >
          <Image height={{ base: "5vh", md: "10vh" }} src={XLAFF_LOGO} />
          <Box w={"full"}>
            <Flex justify={"center"}>
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
            </Flex>
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
