import React from "react";
import {
  Stack,
  Flex,
  Button,
  Text,
  Image,
  VStack,
  useBreakpointValue,
  Box,
  Container,
} from "@chakra-ui/react";
import { SocialIcon } from "react-social-icons";

//img
import XLAFF_LOGO from "../../Assets/img/XLAFF-Logo.png";
import HOME_BG_1 from "../../Assets/img/HOME-BG-1.png";
import HOME_BG_2 from "../../Assets/img/HOME-BG-2.png";
import HOME_BG_3 from "../../Assets/img/HOME-BG-3.png";

export default function Index() {
  return (
    <Box w={"full"} paddingTop="10vh">
      <Box
        w={"full"}
        // h={"100vh"}
        backgroundImage={HOME_BG_1}
        backgroundSize={{ base: "cover", md: "cover", xl: "100%" }}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "2rem", md: "5rem" }}
          pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "5vw", md: "5vw", xl: "5.5vw" }}
            mb="2rem"
          >
            MOVE TO META
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "4vw", md: "4vw", xl: "3.5vw" }}
          >
            <Text>MARKETING</Text>
            <Text>OF THE FUTURE !</Text>
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "2vw", md: "1.5vw", xl: "1.2vw" }}
            mb="1.5rem"
          >
            <Text>
              เปิดตัวอภิมหาโปรเจกต์ระดับโลก MOVE TO META
              โลกเสมือนที่เต็มไปด้วยจินตนาการ
            </Text>
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3vw", md: "3vw", xl: "2.5vw" }}
            mb="1.5rem"
          >
            <Text>THE WORLD'S</Text>
            <Text>FIRST</Text>
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3.5vw", md: "3.5vw", xl: "3vw" }}
          >
            <Text>DECENTRALIZED</Text>
            <Text>AFFILIATE PLATFORM</Text>
          </Text>
        </Container>
      </Box>
      <Box
        w={"full"}
        // h={"100vh"}
        backgroundImage={HOME_BG_2}
        backgroundSize={"cover"}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3vw", md: "3vw", xl: "2vw" }}
            mb="2rem"
          >
            / Featured & recommended
          </Text>
          <Box
            w={"full"}
            h="80%"
            align={"center"}
            mb={{ base: "1rem", md: "2rem" }}
          >
            <Image width={{ base: "80%", md: "60%" }} src={HOME_BG_3} />
          </Box>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3vw", md: "3vw", xl: "2vw" }}
            mb={{ base: "0.5rem", md: "2rem" }}
          >
            <Text>Investor announcement</Text>
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "1.3vw", md: "1.1vw", xl: "1vw" }}
            mb={{ base: "0.5rem", md: "2rem" }}
          >
            <Text>
              We are excited to share the news with XLAFF today that XLAFF has
              completed a US$6 million seed round, co-led by DeFiance Capital
              and Hashed. We also received backing from Pantera Capital,
              Coinbase Ventures, Alameda Research, Animoca Brands, Dapper Labs,
              Play Ventures, Coin98 Ventures and SkyVision Capital.
            </Text>
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "3vw", md: "3vw", xl: "2vw" }}
            mb={{ base: "10rem", md: "20rem" }}
          >
            <Text>/ Supported Games</Text>
          </Text>
          <Text
            color={"#fff"}
            fontFamily={"aAtmospheric"}
            fontSize={{ base: "2.5vw", md: "2vw", xl: "1.2vw" }}
          >
            <Text>JOIN US</Text>
            <Flex mt={{ base: "0.5rem", md: "1rem" }}>
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#FFC300"
                style={{ marginRight: "0.6rem", width: "2rem", height: "2rem" }}
              />
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#FFC300"
                style={{ marginRight: "0.6rem", width: "2rem", height: "2rem" }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#FFC300"
                style={{ marginRight: "0.6rem", width: "2rem", height: "2rem" }}
              />
            </Flex>
          </Text>
        </Container>
      </Box>
      <Box
        w={"full"}
        // h={"100vh"}
        bg={"black"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pb={{ base: "2rem", md: "3rem" }}
        >
          <Image height={{ base: "5vh", md: "10vh" }} src={XLAFF_LOGO} />
          <Box w={"full"}>
            <Flex justify={"center"}>
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
            </Flex>
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
