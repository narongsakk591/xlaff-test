import React from "react";
import {
  Stack,
  Flex,
  Button,
  Text,
  Input,
  Image,
  VStack,
  useBreakpointValue,
  Box,
  Container,
  Grid,
  Checkbox,
  Avatar,
} from "@chakra-ui/react";
import { SocialIcon } from "react-social-icons";

//img
import XLAFF_LOGO from "../../Assets/img/XLAFF-Logo.png";
import HOME_BG_2 from "../../Assets/img/HOME-BG-2.png";
import GoogleIcon from "../../Assets/img/google.png";

export default function Index() {
  return (
    <Box w={"full"} paddingTop="10vh">
      <Box
        w={"full"}
        h={"100vh"}
        backgroundImage={HOME_BG_2}
        backgroundSize={"cover"}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"center"}
      >
        <Grid
          templateColumns={{ base: "repeat(1, 1fr)", xl: "repeat(2, 1fr)" }}
          gap={0}
          pt={{ base: "1rem", md: "2rem" }}
          pb={{ base: "2rem", md: "5rem" }}
        >
          <Box
            w={{ base: "100%", xl: "100%" }}
            pl={{ base: "5vw", xl: "15vw", "2xl": "20vw" }}
            pr={{ base: "5vw", xl: "5vw", "2xl": "5vw" }}
            pt={{ base: "5vw", xl: "6vw", "2xl": "7vw" }}
          >
            <Text
              color={"#fff"}
              fontFamily={"aAtmospheric"}
              fontSize={{ base: "2vw", md: "2.5vw", xl: "1.7vw" }}
              mb="2rem"
            >
              <Text>LEVEL UP YOUR</Text>
              <Text>METAVERSE JOURNEY</Text>
            </Text>
            <Text
              color={"#fff"}
              fontFamily={"aAtmospheric"}
              fontSize={{ base: "1.8vw", md: "1.4vw", xl: "0.7vw" }}
              mb={{ base: "0.5rem", md: "2rem" }}
            >
              <Text>METAVERSE JOURNEY</Text>
              <Text>
                Create. Link. Earn. Set up a GFID, start tracking your
                achievement across the Metaverse, discover curated games, access
                gaming tools, and guilds' scholarships, and maximize your
                rewards today at XLAFF.
              </Text>
            </Text>
            <Box w={"full"} mb={{ base: "0.5rem", md: "2rem" }}>
              <Flex justify={"center"} mt={{ base: "0.6rem", md: "1rem" }}>
                <SocialIcon
                  url="https://twitter.com/jaketrent"
                  bgColor="#FFC300"
                  style={{
                    marginRight: "0.6rem",
                    width: "2rem",
                    height: "2rem",
                  }}
                />
                <SocialIcon
                  url="https://web.telegram.org/k/"
                  bgColor="#FFC300"
                  style={{
                    marginRight: "0.6rem",
                    width: "2rem",
                    height: "2rem",
                  }}
                />
                <SocialIcon
                  url="https://discord.com/"
                  bgColor="#FFC300"
                  style={{
                    marginRight: "0.6rem",
                    width: "2rem",
                    height: "2rem",
                  }}
                />
              </Flex>
            </Box>
          </Box>
          <Box
            w="100%"
            h="100vh"
            bg="black"
            mt={{ base: "1rem", xl: "-2rem" }}
            align="center"
            pr={{ base: "5vw", xl: "15vw", "2xl": "20vw" }}
            pl={{ base: "5vw", xl: "5vw", "2xl": "5vw" }}
          >
            <Image
              width={{ base: "22vw", xl: "15vw" }}
              src={XLAFF_LOGO}
              mb={{ base: "-3vw", md: "-3vw", xl: "-2.7vw" }}
            />
            <Text
              color={"#fff"}
              fontFamily={"aAtmospheric"}
              fontSize={{ base: "2.4vw", md: "2.4vw", xl: "1.5vw" }}
              mb="2rem"
            >
              <Text>Register</Text>
            </Text>
            <Stack textAlign={"start"} color={"#fff"}>
              <Text
                fontFamily={"aAtmospheric"}
                fontSize={{ base: "2.4vw", md: "2vw", xl: "1vw" }}
              >
                <Text>Email</Text>
              </Text>
              <Input
                variant="outline"
                borderColor="#000"
                placeholder="Enter Your Email"
                mb={10}
              />
              <Text
                color={"#fff"}
                fontFamily={"aAtmospheric"}
                fontSize={{ base: "2.4vw", md: "2vw", xl: "1vw" }}
                pt={5}
              >
                <Text>Password</Text>
              </Text>
              <Input
                variant="outline"
                borderColor="#000"
                placeholder="Enter Confirm Password"
              />
              <Text
                color={"#fff"}
                fontFamily={"aAtmospheric"}
                fontSize={{ base: "2.4vw", md: "2vw", xl: "1vw" }}
                pt={5}
              >
                <Text>Confirm Password</Text>
              </Text>
              <Input
                variant="outline"
                borderColor="#000"
                placeholder="Enter Your Password"
              />
              <Checkbox
                fontFamily={"aAtmospheric"}
                fontSize={{ base: "2.4vw", md: "2vw", xl: "1vw" }}
              >
                Remember me
              </Checkbox>
            </Stack>
            <Stack align={"center"} color={"#fff"} pt={5}>
              <Button
                width={"30%"}
                borderRadius={20}
                bg="linear-gradient(to right, #00f1d0, #e214fe)"
              >
                REGISTER
              </Button>
              <Text>or</Text>
              <Button
                width={"80%"}
                borderRadius={20}
                bg="linear-gradient(to right, #ff063c, #556cee)"
              >
                <Avatar
                  size="sm"
                  name="Kent Dodds"
                  src={GoogleIcon}
                  bg="rgb(0,0,0,0)"
                  mr="1rem"
                />
                SIGN UP WITH GOOGLE
              </Button>
            </Stack>
          </Box>
        </Grid>
      </Box>

      <Box
        w={"full"}
        // h={"100vh"}
        bg={"black"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pb={{ base: "2rem", md: "3rem" }}
        >
          <Image height={{ base: "5vh", md: "10vh" }} src={XLAFF_LOGO} />
          <Box w={"full"}>
            <Flex justify={"center"}>
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#3EC5FF"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
            </Flex>
          </Box>
        </Container>
      </Box>
    </Box>
  );
}
